# Birdies

This is the repo for the Birds section from [What's at Hand](https://daveriedstra.com/whats-at-hand). Currently it's very messy and contains several various attempts.

The basic premise of this is to create digital artefacts with Pure Data. In the recordings these are very high and pretty quiet, but the patch allows them to be played lower. Make sure you take breaks while you fiddle with this, these high tones can do a number on your ears.

[discussion](https://llllllll.co/t/puredata-thread/7291/480) and [video demonstration](https://vimeo.com/397799230).
